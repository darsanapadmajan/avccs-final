import Vue from 'vue'
import firebase from 'firebase';
import Router from 'vue-router'
import Home from './views/Home.vue'
import Userz from './views/userbody.vue'
import Admin from './views/admin.vue'
import First from './views/firstpage.vue'

import Login from './views/Login.vue'
import BootstrapVue from 'bootstrap-vue'
import Adminlog from './views/Adminlogin.vue'


import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(Router)
Vue.use(BootstrapVue)

let router = new Router({

  routes: [{
      path: '/',
      name: 'home',
      component: Home,

    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import( /* webpackChunkName: "about" */ './views/About.vue')
    },

    {
      path: '/userbody',
      name: 'userz',
      component: Userz,
      meta: {
        is_admin: false,
        requiresAuth: true,

      }
    },

    {
      path: '/firstpage',
      name: 'firstpage',
      component: First,

    },

    {
      path: '/login',
      name: 'Login',
      component: Login,

    },
    {
      path: '/login-admin',
      name: 'Adminlog',
      component: Adminlog,

    },

    {
      path: '/admin',
      name: 'admin',
      component: Admin,
      meta: {

        is_admin: true,
        requiresAuth: true
      }
    }



  ]
});
router.beforeEach((to, from, next) => {
  console.log('to>>>>>>>>', to);
  if (to.matched.some(record => record.meta.requiresAuth)||firebase.auth().currentUser) {
    /* check if not logd in */
    if (!firebase.auth().currentUser) {
      next({
        path: '/firstpage',
      });
      console.log('>>> redirect to loginn  to', to);
    }
    // check if loged in
    else if (firebase.auth().currentUser) {
      console.log('login');
      if (localStorage.user == 'admin') {
        console.log('>>>>> redirect to admin to', to);
        if (to.path == '/admin') {
          next();
          console.log('ok');
        } else {
          console.log('redirect to admin');
          next({
            path: '/admin',
          });
        }

      }
      else
      if (localStorage.user == 'user') {
        console.log('>>>>> redirect to user to', to);

        if (to.path == '/userbody') {
          console.log('ok');
          next();

        } else {
          console.log('redirect to user');
          next({
            path: '/userbody',
          });
        }
      }
    }
  }
  else{
    next();

  }
});
export default router;