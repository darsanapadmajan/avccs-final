import Vue from 'vue'; 
import 'bootstrap-css-only/css/bootstrap.min.css'; 
import 'mdbvue/build/css/mdb.css';
import App from './App.vue'
import router from './router'
import BootstrapVue from 'bootstrap-vue'
import Hea from './views/headd.vue'
import Usr from './views/userbody.vue'
import first from './views/firstpage.vue'
import admin from './views/admin.vue'
import firebase, { auth } from 'firebase'
import './views/firebaseInit';
import foot from './views/footerr.vue'
Vue.config.productionTip = false



Vue.component('H',Hea)
Vue.component('firsty',first)
Vue.component('Ubody',Usr)
Vue.component('admin',admin)
Vue.component('foo',foot)
Vue.use(BootstrapVue)

let app;
firebase,auth().onAuthStateChanged(user => {
  if(!app) {
    app = new Vue({
      router,
      render: h => h(App)
    }).$mount('#app')
    
  }
})
